<?php

namespace core\amo;

use Introvert\ApiClient;
use Introvert\ApiException;
use Introvert\Configuration;
use DateTime;
use DateTimeZone;
use Exception;

class Client extends ApiClient
{
    public $auth = false;
    public $accountInfo = [];
    public $errors = [];

    public function __construct(string $key = null, Configuration $config = null)
    {
        if ($key) {
            Configuration::getDefaultConfiguration()->setApiKey('key', $key);
        }
        parent::__construct($config);
        try {
            $this->accountInfo = $this->account->info()['result'];
            $this->auth = true;
        } catch (ApiException $e) {
            $this->errors[] = $e->getMessage();
        }
    }

    /**
     * Доступные сущности api
     *
     * @var array
     */
    protected $apis = [
        'account' => 'AccountApi',
        'bp' => 'BpApi',
        'company' => 'CompanyApi',
        'contact' => 'ContactApi',
        'customer' => 'CustomerApi',
        'lead' => 'Lead',
        'links' => 'LinksApi',
        'mail' => 'MailApi',
        'note' => 'NoteApi',
        'pbx' => 'PbxApi',
        'sms' => 'SmsApi',
        'task' => 'TaskApi',
        'telegram' => 'TelegramApi',
        'transaction' => 'TransactionApi',
        'yadro' => 'YadroApi',
    ];

    public function __get($name)
    {
        if (isset($name, $this->apis)) {
            if (isset($this->{$name})) {
                return $this->{$name};
            } else {
                switch ($name){
                    case 'lead' :
                        $apiClass = '\core\amo\Api\\'. $this->apis[$name];
                    break;
                    default :
                        $apiClass = '\Introvert\Api\\'. $this->apis[$name];
                     break;
                }
                $this->{$name} = new $apiClass($this);
                return $this->{$name};
            }
        }

        return false;
    }
}
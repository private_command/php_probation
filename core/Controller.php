<?php


namespace core;

use ReflectionException;
use ReflectionClass;
class Controller
{
    public $layout = 'layout';
    public $themesPath = __DIR__ . '/../application/themes';

    /**
     * Редирект
     * @param string $url Урл куда редиректить
     */
    public function redirect($url)
    {
        header("Location: {$url}");
        die();
    }


    /**
     * Подключение верстки
     * @param string $view имя файла вертстки без .php
     * @param array $data Массив данных для подстановки в верстку
     * @throws ReflectionException
     */
    public function render($view, $data = [])
    {
        $class = new ReflectionClass($this);
        $content = null;
        extract($data);
        ob_start();
        require($this->themesPath . '/views/' . strtolower($class->getShortName()) . '/' . $view . '.php');
        //Если Ajax представление без макета
        if (!$this->isAjax()) {
            $content = ob_get_clean();
            require((string)$this->themesPath . '/layouts/' . (string)$this->layout . '.php');
        }
    }

    /**
     * Поверка на ajax запрос
     * @return bool
     */
    public function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower(getenv('HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest'));
    }

}
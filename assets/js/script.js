(function($) {
    jQuery.ajax({
        url:  "/subscribes/calendar",
        dataType: "json",
        beforeSend: function(){
            $('.loading').css('display','block');
        },
        complete: function(data) {
            var disabled = [];
            $.each(data.responseJSON, function(index, value) {
                disabled.push({date: new Date(value)});
            });
            $('#calendar').glDatePicker({
                noSelectableDates: disabled
            }).glDatePicker(true);
            $('.loading').css('display','none');
        }
    });
})(jQuery);


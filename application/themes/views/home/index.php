<table class="table">
      <thead>
      <tr>
          <th scope="col">#</th>
          <th scope="col">Имя</th>
          <th scope="col">Бюджет</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($out as $item) : ?>
          <tr>
              <th scope="row"><?=$item['id']?></th>
              <td><?=$item['name']?></td>
              <td><?=$item['budget']?></td>
          </tr>
      <?php endforeach; ?>
      <tr>
          <th scope="row">Всего</th>
          <td></td>
          <td><?=$total?></td>
      </tr>
      </tbody>
  </table>
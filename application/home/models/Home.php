<?php


namespace application\home\models;


use core\amo\Client;
use core\amo\TestData;

class Home
{
    /**
     * @param int $from
     * @param int $to
     * @return array
     * @throws \Introvert\ApiException
     */
    public function dateFromToFilter(int $from = 0, int $to = 0): array
    {
        $clients = (new TestData())->getClients();
        $out = [];
        $total = 0;
        foreach ($clients as $client) {
            $api = new Client($client['api']);
            if ($api->auth) {
                $leads = $api->lead->getMyAll(null, 142);
                $budget = $this->totalBudget($leads, $from, $to);
                $out[] = [
                    'id' => $api->accountInfo['id'],
                    'name' => $api->accountInfo['name'],
                    'budget' => $budget,
                ];
                $total += $budget;
            } else {
                $out[] = [
                    'id' => $client['id'],
                    'name' => $client['name'],
                    'budget' => 'Не активен',
                ];
            }
        }
        return ['out' => $out, 'total' => $total];
    }

    /**
     * @param int|null $from
     * @param int|null $to
     * @return float|int
     */
    private function totalBudget(array $leads, int $from = null, int $to = null): float
    {

        if ($from) {
            $leads = array_filter($leads, function ($el) use ($from) {
                return $el['date_close'] >= $from;
            });
        }
        if ($to) {
            $leads = array_filter($leads, function ($el) use ($to) {
                return $el['date_close'] <= $to;
            });
        }
        $total = 0;
        foreach ($leads as $lead) {
            $total += (float)$lead['price'];
        }
        return $total;
    }
}
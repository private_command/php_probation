<?php


namespace application\home;


use application\App;

class Router extends \Zaphpa\BaseMiddleware
{
    function preprocess(&$router) {
        $router->addRoute(array(
            'path'     => "/",
            'get'      => ['\application\home\controllers\Home', 'index'],
            'handlers' => array(
                'from'         => \Zaphpa\Constants::PATTERN_NUM,
                'to'         => \Zaphpa\Constants::PATTERN_NUM,
            ),
        ));
        $router->addRoute(array(
            'path'     => "/home",
            'get'      => ['\application\home\controllers\Home', 'index'],
            'handlers' => array(
                'from'         => \Zaphpa\Constants::PATTERN_NUM,
                'to'         => \Zaphpa\Constants::PATTERN_NUM,
            ),
        ));
    }
    function preroute(&$req, &$res) {
            $req->params['action'] = self::$context['pattern'];
    }
}

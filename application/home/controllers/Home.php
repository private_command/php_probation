<?php


namespace application\home\controllers;
use core\Controller;
use application\home\models\Home as HomeModel;

class Home extends Controller
{
    public function index($req, $res)
    {

        $from = !empty($req->data['from'])? filter_var($req->data['from'], FILTER_VALIDATE_INT):0;
        $to = !empty($req->data['to'])? filter_var($req->data['to'], FILTER_VALIDATE_INT):0;
        $model = new HomeModel();
        extract($model->dateFromToFilter($from,$to));
        return $this->render('index',[
            'title'=>'Первая задача. Бюджеты.',
            'action'=>$req->params['action'],
            'out'=>$out,
            'total'=>$total,
        ]);
    }
}
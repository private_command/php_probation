<?php


namespace application\subscribes\models;


use core\amo\Client;
use DateTime;

class Sudscribe
{
    private $api;

    private $maxCountInDay = 5;

    protected $config = [];

    public function __construct()
    {
        $this->api = new Client('');

    }


    public function getField(int $fieldId, array $statuses = null, $pipelineId = null) : string
    {
        $date = (new DateTime())->modify('-1 Month')->format('d.m.Y');
        //Выбор лидов за месяц со статусами $statuses
        $leads = $this->api->lead->getMyAll($date, $statuses);
        //Фильтр по воронке
        if ($pipelineId) {
            $leads = array_filter($leads, function ($el) use ($pipelineId) {
                if(is_array($pipelineId)){
                    return in_array($el['pipeline_id'],$pipelineId);
                }elseIf(is_int($pipelineId)){
                    return $el['pipeline_id'] == (int)$pipelineId;
                }else{
                    throw new Exception("pipelineId не число и не массив");
                }

            });
        }
        //Выбор по кастомному полю $fieldId (замороченно, но что делать, массив лидов громоздкий)
        $arrayiterator = new \RecursiveArrayIterator($leads);
        $iterator = new \RecursiveIteratorIterator($arrayiterator, \RecursiveIteratorIterator::CHILD_FIRST);
        $dates = [];
        foreach ($iterator as $key => $value) {
            //Фильтр по полю $fieldId
            if ($key !== 'id' || $value !== $fieldId) continue;
            //Получаем массив поля
            //@TODO Сделать универсально с учетом особенностей типов полей вроде multiple
            $fieldArray = $iterator->getArrayCopy();
            $values = !empty($fieldArray['values']) ? reset($fieldArray['values']) : [];
            if (!$values) continue;
            $date = !empty($values['value']) ? $values['value'] : '';
            if (!$date) continue;
            //Формат даты для календаря
            $dates[] = (new DateTime($date))->format('m/d/Y');
        }

        //Фильтр по количеству сделок с одной датой если на одну дату больше $this->maxCountInDay сделок заносим в массив
        $out = array_filter(array_count_values($dates), function ($el) {
            return $el >= $this->maxCountInDay;
        });
        //Последствия array_count_values. Даты из ключей получаем
        $out = array_keys($out);
        //Массив в формат json для js
        return json_encode($out);
    }

}
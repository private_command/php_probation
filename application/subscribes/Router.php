<?php


namespace application\subscribes;


use application\App;

class Router extends \Zaphpa\BaseMiddleware
{
    function preprocess(&$router) {
        $router->addRoute(array(
            'path'     => "/subscribes",
            'get'      => ['\application\subscribes\comtrollers\Subscribe', 'index'],
        ));
        $router->addRoute(array(
            'path'     => "/subscribes/calendar",
            'get'      => ['\application\subscribes\comtrollers\Subscribe', 'calendar'],
        ));
    }

    function preroute(&$req, &$res) {
        $req->params['action'] = self::$context['pattern'];
    }
}

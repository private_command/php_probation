<?php


namespace application;


class App
{

    public function run()
    {

        if (!isset($_SESSION['csrf_token'])) {
            $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
        }
        //Проверка csrf_token-а защита от csrf атак
        if(!self::checkCsrfToken()){
            throw new \Exception('В $_POST отсутстует csrf_token', 403);
        }
        //Инициализация роутера
        $router = new \Zaphpa\Router();
        $router->attach('\Zaphpa\Middleware\AutoDocumentator', '/apidocs'); //auto-docs middleware
        $router->attach('\Zaphpa\Middleware\MethodOverride');
        $router->attach('\Zaphpa\Middleware\CORS');

        //Подключение роутеров в каталогах
        //Home
        $router->attach('\application\home\Router');

        //subscribes
        $router->attach('\application\subscribes\Router');

        //Подключение доступа к каталогам по url
        //Корень "/en/"
        $router->addRoute([
            'path' => "/",
            'get' => function () {
                return true;
            }, // Доступен по get
            'post' => function () {
                return true;
            } // Доступен по post
        ]);

        $router->addRoute(array(
            'path' => "/subscribes",
            'get' => function () {
                return true;
            },
        ));

        try {
            $router->route();
        } catch (\Zaphpa\InvalidPathException $ex) {
            header('Status: 403 Forbidden');
            header('HTTP/1.1 403 Forbidden');
            $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
            die();
        }
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }

    private static function checkCsrfToken()
    {
        return true;
    }

}

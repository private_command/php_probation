<?php
ini_set ('error_log', __DIR__.'/my_php_errors.log');
ini_set('memory_limit', '512M');
if(!session_id()) session_start();
require __DIR__ . '/vendor/autoload.php';
(new application\App)->run();